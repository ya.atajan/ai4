import re
import csv
import nltk
import Porter_Stemmer_Python
from nltk.tokenize import word_tokenize
from collections import Counter

ps = nltk.stem.PorterStemmer()
regex_alphanumeric = re.compile('[^a-zA-Z0-9]')
regex_no_num = re.compile('\d+')
original = []
tokenizer_sentences = []
stopwords = []
final = []
report = []


# Get stopwords
def stop_word_getter(src):
    with open(src) as f:
        for line in f:
            stopwords.append(line.rstrip())


# Tokenize sentences
def tokenize(src):
    global original
    with open(src) as f:
        for line in f:
            tokens = word_tokenize(line.rstrip())
            if len(tokens) > 0:
                tokenizer_sentences.append(tokens)

    original = tokenizer_sentences


# Remove special characters
def remove_special_char(src):
    for tokens in src:
        i = 0
        for token in tokens:
            new_token = regex_alphanumeric.sub('', token)
            if len(new_token) == 0:
                del tokens[i]
            else:
                tokens[i] = new_token
            i += 1


# Remove numbers
def remove_num(src):
    for tokens in src:
        i = 0
        for token in tokens:
            new_token = regex_no_num.sub('', token)
            if len(new_token) == 0:
                del tokens[i]
            else:
                tokens[i] = new_token
            i += 1


# Lowercase all
def to_lower(src):
    for tokens in src:
        i = 0
        for token in tokens:
            tokens[i] = token.lower()
            i += 1


# Remove numbers
def remove_stopwords(src):
    for tokens in src:
        i = 0
        for token in tokens:
            for stop_word in stopwords:
                if stop_word in token:
                    new_token = token.replace(stop_word, '')
                    if len(new_token) == 0:
                        del tokens[i]
                    else:
                        tokens[i] = new_token
            i += 1


# Stemming
def stem(src):
    for tokens in src:
        i = 0
        for token in tokens:
            tokens[i] = ps.stem(token)
            i += 1


# Stemming with provided porter_stemmer...
def stem_provided(src):
    stemer = Porter_Stemmer_Python.PorterStemmer()
    for tokens in src:
        i = 0
        for token in tokens:
            tokens[i] = (stemer.stem(token,0,len(token)-1))
            i += 1


# Combine stemmed words
def combine(src):
    for tokens in src:
        i = 0
        for token in tokens:
            if token not in final:
                final.append(token)
            i += 1


# Create TDM csv
def vector(keywords):
    report.append(keywords)

    for line in tokenizer_sentences:
        insert = [0] * len(keywords)
        index = 0
        count = 0
        for keyword in keywords:
            for word in line:
                if word == keyword:
                    count += 1
            insert[index] = count
            index += 1
        report.append(insert)

    write_csv(report)


def write_csv(data):
    file = open('TDM.csv', 'w+')
    with file:
        writer = csv.writer(file)
        writer.writerows(data)

    print("Writing complete")



stop_word_getter("stop_words.txt")
tokenize("sentences.txt")
# print(tokenizer_sentences)
# print(stopwords)
remove_special_char(tokenizer_sentences)
remove_num(tokenizer_sentences)
to_lower(tokenizer_sentences)
remove_stopwords(tokenizer_sentences)
stem(tokenizer_sentences)
combine(tokenizer_sentences)
print(final)
vector(final)
print(len(report))

